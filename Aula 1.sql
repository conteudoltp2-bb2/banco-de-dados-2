-- O duplo sinal de menos comenta toda a linha a partir do mesmo

/* e */ /* -- Comenta diversas linhas, ou seja um bloco

Duas formas de execução das queries:
1) Como instrução - gerar um resultado em forma de tabela
2) Como script - gerar resultado em texto
*/

SELECT * -- * Está indicando que a QUERY irá trazer todas as colunas da tabela
FROM EMP; -- Toda query termina com ponto-vírgula

SELECT FIRST_NAME
FROM EMP;

/*
SELECT -- Projeta as colunas desejadas existentes na origem FROM, separadas por vírgula
FROM -- Relaciona a origem onde estão os dados a serem consultados 
WHERE -- Faz restrição (filtro) das linhas mostradas conforme uma condição 
    -- Operadores para definir uma condição: =, <> (!+), >=, <=, <, >, NOT, AND, OR
    --                                      BETWEEN, LIKE, IN
GROUP BY -- Estabelecer subgrupos no resultado da QUERY
HAVING -- Faz a restrição (filtro) os subgrupops formados conforme uma condição
ORDER BY -- Ordena o resultado apresentado, ascendente ou descendente por coluna
*/

-- Produzindo resultados constantes 
SELECT TO_CHAR(ROUND(123456 / 65432)) AS CALC1,
    TO_CHAR(SYSDATE, 'YYYY-DD-MM') "DATA HOJE",
    SYSDATE "DATA HOJE",
    'Teste' AS STRING,
    5 AS NUMERO,
FROM DUAL; -- DUAL é um objeto virtual equivalente a uma tabela sem colunas no BD

-- Produzindo resultados constantes a partir de uma tabela com dados no BD
SELECT TO_CHAR(ROUND(123456 / 65432)) AS CALC1,
    TO_CHAR(SYSDATE, 'YYYY-DD-MM') "DATA HOJE",
    SYSDATE "DATA HOJE",
    'Teste' AS STRING,
    5 AS NUMERO,
    FIRST_NAME
FROM EMP; 

-- Veificando e analisando metadados no dicionário de dados dinâmicos (catálogo)
DESC ALL_OBJECTS; -- DESC descreve a estrututra de uma tabela ou view

-- Selecionando os dados da VIEW ALL_OBJECTS
SELECT *
FROM ALL_OBJECTS;

SELECT COUNT(*) -- Contando a quantidade de objetos que o usuário pode ver
FROM ALL_OBJECTS;

SHOW USER;

SELECT DISTINCT OBJECT_TYPE -- Verificando os objetos distintos existentes no BD
FROM ALL_OBJECTS;

-- Verificar os objetos existentes a partir de um filtro com palavras de interesse
SELECT OBJECT_NAME
FROM ALL_OBJECTS;
WHERE OBJECT_TYPE='VIEW'; -- Os dados armazenados no BD são sensíveis a maiuscula/minuscula

-- Buscando informações sobre tabelas no catálogo (DD)
SELECT OBJECT_NAME
FROM ALL_OBJECTS
WHERE OBJECT_TYPE='VIEW'
    AND OBJECT-NAME LIKE 'TABLE%';
    
                            -- 
                            -- % Substitui uma cadeia de caracteres
                            -- _ Substitui uma posição de um caracter
                            
-- Buscando informações relacionadas a tabelas no catálogo (DD)
SELECT OBJECT_NAME
FROM ALL_OBJECTS
WHERE OBJECT_TYPE='VIEW'
    AND OBJECT-NAME LIKE '___TAB%COL%';
        OR OBJECT_NAME LIKE 'U%TAB%COL%';
        
-- Buscando os dados de uma VIEW relacionadas às tabelas e colunas
SELECT *
FROM USER_TAB_COLUMNS;

-- 